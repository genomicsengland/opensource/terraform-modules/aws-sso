# Find the AWS SSO application in Okta
data "okta_app" "aws_sso" {
  label       = "AWS SSO in ${title(var.environment)} Org"
  active_only = true
}

# Okta -> AWS Mapping Table
data "aws_dynamodb_table" "aws_sso_mapping" {
  name = "SSO-Group-Mapping"
}