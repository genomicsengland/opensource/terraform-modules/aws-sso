resource "aws_dynamodb_table_item" "aws_sso_mapping" {
  for_each = var.ad_groups

  table_name = data.aws_dynamodb_table.aws_sso_mapping.name
  hash_key   = data.aws_dynamodb_table.aws_sso_mapping.hash_key
  item = jsonencode({
    pk             = { S = "${var.account_id}#${okta_group.group[each.key].name}" }
    Account        = { S = var.account_id }
    Group          = { S = okta_group.group[each.key].name }
    PermissionSets = { S = join(",", each.value) }
  })
}