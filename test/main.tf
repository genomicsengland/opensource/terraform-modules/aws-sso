terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.9.0"
    }

    okta = {
      source  = "okta/okta"
      version = "~> 3.10"
    }
  }
  required_version = ">= 1.0"
}

provider "aws" {
  alias = "account"
  assume_role {
    role_arn = "arn:aws:iam::018829343244:role/OrgDeploy"
  }
}

provider "okta" {
  org_name  = "ngisengland"
  base_url  = "okta.com"
  api_token = data.aws_secretsmanager_secret_version.okta_token.secret_string
}

# Okta API Token (for the Okta provider)
data "aws_secretsmanager_secret_version" "okta_token" {
  secret_id = "arn:aws:secretsmanager:eu-west-2:512426816668:secret:/prod/root/okta/api_token-PGwr94"
}


module "sso_sandbox_bparker_admin" {
  source = "../"

  environment = "test"
  account_id  = "742545482708"
  ad_groups = {
    APP-AWS-SSO-Access-Sandbox-Bparker = ["AdministratorAccess", "Billing"]
  }
}
