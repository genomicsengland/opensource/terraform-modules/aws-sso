# AWS-SSO

Terraform module to create SSO mappings from GEL AD to AWS accounts

> Note: The AWS Account, AD Groups and AWS PermissionSets referenced in this module _must_ exist.

## Example Usage

```json
module "sso_sandbox_bparker" {
  source = "git::https://gitlab.com/genomicsengland/opensource/terraform-modules/aws-sso.git?ref=2021.06.1"

  environment = var.root
  account_id  = var.root == "prod" ? "025403892336" : "742545482708"
  ad_groups   = {
    APP-AWS-SSO-Access-Sandbox-Bparker = ["AdministratorAccess", "Billing"]
  }
}
```

## Argument Reference

All arguments are required

| Argument | Description |
|---|---|
| `environment` | the name of the organisation to which the current deployment is targeted (`prod` or `test`) - this should be from a variable such as `var.root` |
| `account_id` | The ID of the target AWS Account - should be taken from the output of the `aws-account` module |
| `ad_groups` | a map of AD groups and which premission sets they map to.  See [AD Group Maps](#AD-Group-Maps)

### AD Group Maps

Active directory groups are mapped to one or more AWS PermissionSets.  
The `ad_groups` parameter should be a map where each key is the name of an AD group, and its value is a List of PermissionSet names, e.g.

```json
  ad_groups = {
    APP_SSO_Access_My_Active_Directory_Group = ["PermissionSet1", "PermissionSet2"]
    APP_SSO_Access_Another_AD_Group          = ["PermissionSet3"]
  }
```

In this example, members of `APP_SSO_Access_My_Active_Directory_Group` will get two entries in the AWS SSO page allowing them to access the account using _either_ `PermissionSet1` _or_ `PermissionSet2`, but not both at the same time.

Both the Active Directory groups and the AWS PermissionSets _must_ exist otherwise the Terraform Plan will fail.

## Attributes Reference

None.
