variable "environment" {
  type        = string
  description = "Organisation being deployed to - prod or test"
}

variable "account_id" {
  type        = string
  description = "AWS Account ID where the AD groups will attach permissions"
}

variable "ad_groups" {
  type        = map(any)
  description = "Map containing a list of AD group names as keys, each having a value of a List of strings of permission set names"
}