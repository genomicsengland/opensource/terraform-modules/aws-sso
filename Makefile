AWS_REGION				:= eu-west-2
DIR_TERRAFORM			:= test
FILE_CREDENTIALS_DOCKER	:= /tmp/credentials
FILE_CREDENTIALS_HOST	:= $(HOME)/.aws/credentials
TF_VERSION				:= latest
TF_IMAGE				:= hashicorp/terraform:$(TF_VERSION)
TF_BIND_DIR				:= /terraform

ifneq (,$(wildcard $(FILE_CREDENTIALS_HOST)))
	MOUNT_CREDENTIALS_FILE := -v $(FILE_CREDENTIALS_HOST):$(FILE_CREDENTIALS_DOCKER)
endif
ifeq (1,$(DISABLE_COLOR))
	NO_COLOR	:= -no-color
endif

TF_CMD = docker run \
		 	--rm \
			--user $(shell id -u) \
			--mount type=bind,source="$(shell pwd)",destination=$(TF_BIND_DIR) \
			-w $(TF_BIND_DIR) \
			--env AWS_ACCESS_KEY_ID=$(AWS_ACCESS_KEY_ID) \
			--env AWS_SECRET_ACCESS_KEY=$(AWS_SECRET_ACCESS_KEY) \
			--env AWS_PROFILE=$(AWS_PROFILE) \
			--env AWS_SHARED_CREDENTIALS_FILE=$(FILE_CREDENTIALS_DOCKER) \
			--env AWS_SDK_LOAD_CONFIG=1 \
			--env AWS_DEFAULT_REGION=$(AWS_REGION) \
			$(MOUNT_CREDENTIALS_FILE) \
			$(TF_IMAGE)

init:
	$(TF_CMD) \
		-chdir=$(DIR_TERRAFORM) \
		init \
		$(NO_COLOR)

check-fmt:
	$(TF_CMD) fmt -check .
	$(TF_CMD) fmt -check test

fmt:
	$(TF_CMD) -chdir=$(DIR_TERRAFORM) fmt

plan:
	$(TF_CMD) \
		-chdir=$(DIR_TERRAFORM) \
		plan \
		$(NO_COLOR)

clean:
	rm -rf $(DIR_TERRAFORM)\.terraform

.PHONY: init check-fmt plan shell clean
