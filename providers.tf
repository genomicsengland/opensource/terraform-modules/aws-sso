terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.20.0"
    }

    okta = {
      source  = "okta/okta"
      version = "~> 3.10"
    }
  }
  required_version = ">= 1.0"
}