# Create the Okta group
resource "okta_group" "group" {
  for_each = var.ad_groups

  name = substr(replace(replace(each.key, "Access", "${title(var.environment)}-Org"), "-", "_"), 4, 999)
}

resource "okta_group_rule" "rule" {
  for_each = var.ad_groups

  name              = substr(replace(replace(each.key, "Access", "${title(var.environment)}-Org"), "-", " "), 4, 999)
  status            = "ACTIVE"
  group_assignments = [okta_group.group[each.key].id]
  expression_type   = "urn:okta:expression:1.0"
  expression_value  = "isMemberOfGroupName(\"${each.key}\")"
}

# Assign the Okta group to the Okta AWS application 
resource "okta_app_group_assignment" "aws_sso" {
  for_each = var.ad_groups

  app_id   = data.okta_app.aws_sso.id
  group_id = okta_group.group[each.key].id
}